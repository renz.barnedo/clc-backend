const express = require('express');
const compression = require('compression');
const multer = require('multer');
require('dotenv').config();

const corsController = require('./middleware/setCors');
const errorHandler = require('./middleware/errorHandler');

const deviceRoutes = require('./routes/device');
const logRoutes = require('./routes/log');
const authRoutes = require('./routes/auth');
const personRoutes = require('./routes/person');

const authUser = require('./middleware/userIsAuthenticated');

const sequelize = require('./util/database');
const app = express();

app.use(compression({ level: 9 }));
app.use(corsController);
app.use(express.json());

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, process.env.IMAGES_FOLDER);
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === 'image/png' ||
    file.mimetype === 'image/jpg' ||
    file.mimetype === 'image/jpeg'
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage,
  fileFilter,
});

app.use('/v1/device', deviceRoutes);
app.use('/v1/log', logRoutes);
app.use('/v1/auth', authRoutes);
app.use('/v1/person', personRoutes);

app.use('/v1/upload', upload.single('image'), (req, res, next) => {
  const image = req.file;
  if (!image) {
    throwError('cannot upload image', 401);
  }
  res.status(200).json({
    message: 'image uploaded',
    image,
  });
});
app.use(
  '/v1/images',
  authUser,
  express.static(__dirname + '/' + process.env.IMAGES_FOLDER)
);

app.use(errorHandler);

const PORT = process.env.PORT || 2000;
const message = `Server running on  port ${PORT}`;

sequelize
  .sync()
  .then((result) => {
    // app.listen(PORT);
    app.listen(PORT, console.log(message));
  })
  .catch((error) => {
    console.log(error);
  });
