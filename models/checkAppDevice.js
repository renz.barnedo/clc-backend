const Sequelize = require('sequelize');
const sequelize = require('../util/database');

module.exports = sequelize.define('checkAppDevices', {
  checkAppDeviceId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
  },
  devicename: Sequelize.STRING,
  password: Sequelize.STRING,
  token: Sequelize.STRING,
  expiry: Sequelize.DATE,
  createdAt: Sequelize.DATE,
  updatedAt: Sequelize.DATE,
});
