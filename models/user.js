const Sequelize = require('sequelize');
const sequelize = require('../util/database');

module.exports = sequelize.define('users', {
  userId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
  },
  employeeId: Sequelize.STRING,
  firstName: Sequelize.STRING,
  middleName: Sequelize.STRING,
  lastName: Sequelize.STRING,
  suffixName: Sequelize.STRING,
  department: Sequelize.STRING,
  position: Sequelize.STRING,
  updatedBy: Sequelize.INTEGER,
  createdAt: Sequelize.DATE,
  updatedAt: Sequelize.DATE,
});
