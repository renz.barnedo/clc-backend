const Sequelize = require('sequelize');
const sequelize = require('../util/database');

module.exports = sequelize.define('roles', {
  roleId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
  },
  userId: Sequelize.INTEGER,
  role: Sequelize.STRING,
  username: Sequelize.STRING,
  password: Sequelize.STRING,
  updatedBy: Sequelize.INTEGER,
  createdAt: Sequelize.DATE,
  updatedAt: Sequelize.DATE,
});