const Sequelize = require('sequelize');
const sequelize = require('../util/database');

module.exports = sequelize.define('entries', {
  entryId: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
  },
  userId: Sequelize.INTEGER,
  timeIn: Sequelize.DATE,
  timeOut: Sequelize.DATE,
  qrType: Sequelize.STRING,
  updatedBy: Sequelize.INTEGER,
  createdAt: Sequelize.DATE,
  updatedAt: Sequelize.DATE,
});
