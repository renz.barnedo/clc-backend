const express = require('express');
const authUser = require('../middleware/userIsAuthenticated');
const authDevice = require('../middleware/deviceIsAuthenticated');
const userIsAdmin = require('../middleware/userIsAdmin');

const router = express.Router();

const {
  insertUser,
  insertUsers,
  selectUserWithUserId,
  selectColumnsWhereUsers,
  updateUser,
  deleteUserWhereUserId,
  deleteUsersWhere,
} = require('../controllers/person');

router.post('/user', authUser, userIsAdmin, insertUser);
router.post('/users', authUser, userIsAdmin, insertUsers);

router.get('/user/:userId', authUser, selectUserWithUserId);
router.get('/users', authUser, userIsAdmin, selectColumnsWhereUsers);

router.get('/user/:userId/device', authDevice, selectUserWithUserId);
router.get('/users/device', authDevice, selectColumnsWhereUsers);

router.patch('/user', authUser, updateUser);

router.delete('/user/:userId', authUser, userIsAdmin, deleteUserWhereUserId);
router.delete('/users', authUser, userIsAdmin, deleteUsersWhere);

module.exports = router;
