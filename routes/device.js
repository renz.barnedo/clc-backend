const express = require('express');
const router = express.Router();
const authDevice = require('../middleware/deviceIsAuthenticated');

const { login, selectUsers } = require('../controllers/device');

router.post('/login', login);
router.get('/employees', authDevice, selectUsers);

module.exports = router;
