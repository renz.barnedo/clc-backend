const express = require('express');

const authDevice = require('../middleware/deviceIsAuthenticated');
const authUser = require('../middleware/userIsAuthenticated');
const isAdmin = require('../middleware/userIsAdmin');

const { validateTimeIn, validateTimeOut } = require('../middleware/validate');

const {
  insertEntry,
  insertEntries,
  selectEntryWithEntryId,
  selectColumnsWhereEntries,
  updateEntry,
  deleteEntryWhereEntryId,
  deleteEntriesWhere,
} = require('../controllers/log');

const router = express.Router();

router.post('/entry', authDevice, validateTimeIn, insertEntry);
router.post('/entries', authDevice, insertEntries);

router.get('/entry/:entryId', authDevice, selectEntryWithEntryId);
router.get('/entries', authUser, selectColumnsWhereEntries);

router.patch('/entry', authDevice, validateTimeOut, updateEntry);

router.delete('/entry/:entryId', isAdmin, deleteEntryWhereEntryId);
router.delete('/entries', isAdmin, deleteEntriesWhere);

module.exports = router;
