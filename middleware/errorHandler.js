module.exports = (error, req, res, next) => {
  console.log(error);
  let { status, message, data } = error;
  status = status || 500;
  res.status(status).json({ message: message, data });
};
