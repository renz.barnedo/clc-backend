const sequelize = require('sequelize');
const { Op } = require('sequelize');
const moment = require('moment');

const Entry = require('../models/entry');

const DATE_FORMAT = 'YYYY-MM-DD';
const TIMEIN_PROPERTY = 'timeIn';

exports.validateTimeIn = async (req, res, next) => {
  let timeInToday;
  try {
    timeInToday = await Entry.findOne({
      where: [
        sequelize.where(
          sequelize.fn('DATE', sequelize.col(TIMEIN_PROPERTY)),
          moment(new Date(req.body.data[TIMEIN_PROPERTY])).format(DATE_FORMAT)
        ),
        { userId: req.body.data.userId },
      ],
    });
  } catch (error) {
    throw error;
  }
  if (timeInToday) {
    const error = new Error('time-in exists');
    error.status = 400;
    next(error);
  }
  next();
};

exports.validateTimeOut = async (req, res, next) => {
  const TIMEOUT_PROPERTY = 'timeOut';
  let timeOutToday;
  try {
    timeOutToday = await Entry.findOne({
      where: [
        sequelize.where(
          sequelize.fn('DATE', sequelize.col(TIMEOUT_PROPERTY)),
          moment(new Date(req.body.data[TIMEOUT_PROPERTY])).format(DATE_FORMAT)
        ),
        req.body.where,
      ],
    });
  } catch (error) {
    throw error;
  }
  if (timeOutToday) {
    const error = new Error('time-out exists');
    error.status = 400;
    next(error);
  }
  next();
};
