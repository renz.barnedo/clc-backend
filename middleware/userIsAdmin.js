module.exports = (req, res, next) => {
  if (req.role !== 'admin') {
    const error = new Error('user is unauthorized');
    error.status = 401;
    throw error;
  }
};
