const jwt = require('jsonwebtoken');

const throwError = (message, status, error) => {
  error = error ? error : new Error(message);
  error.status = status;
  throw error;
};

module.exports = (req, res, next) => {
  const authHeader = req.get('Authorization');
  if (!authHeader) {
    throwError('no authorization', 400);
  }
  const token = authHeader.split(' ')[1];
  let decodedToken;
  try {
    decodedToken = jwt.verify(token, process.env.DEVICETOKEN);
  } catch (error) {
    throwError(null, 401, error);
  }
  if (!decodedToken) {
    throwError('device not authenticated.', 422);
  }
  next();
};
