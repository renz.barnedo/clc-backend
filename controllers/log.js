const moment = require('moment');
const Entry = require('../models/entry');
const DATETIME_FORMAT = 'YYYY-MM-DD hh:mm:ss A';

const formatDate = (date) => moment(date).format(DATETIME_FORMAT);
const setEntryDates = (entry) => {
  entry.timeIn = formatDate(entry.timeIn);
  entry.timeOut = formatDate(entry.timeOut);
  entry.updatedAt = formatDate(entry.updatedAt);
  entry.createdAt = formatDate(entry.createdAt);
  return entry;
};

exports.insertEntry = async (req, res, next) => {
  try {
    const insertedEntry = await Entry.create(req.body.data);
    setEntryDates(insertedEntry.dataValues);

    res.status(201).json({
      message: 'inserted entry',
      data: insertedEntry,
    });
  } catch (error) {
    next(error);
  }
};

exports.insertEntries = async (req, res, next) => {
  try {
    const insertedEntries = await Entry.bulkCreate(req.body.data);
    res.status(201).json({
      message: 'inserted entries',
      data: insertedEntries,
    });
  } catch (error) {
    next(error);
  }
};

exports.selectEntryWithEntryId = async (req, res, next) => {
  try {
    const selectedEntry = await Entry.findByPk(req.params.entryId);
    setEntryDates(selectedEntry.dataValues);

    res.status(200).json({
      message: `selected user with entryId: ${req.params.entryId}`,
      data: selectedEntry,
    });
  } catch (error) {
    next(error);
  }
};

exports.selectColumnsWhereEntries = async (req, res, next) => {
  try {
    let queryConditions = {};
    if (req.body) {
      if (req.body.columns) {
        queryConditions.attributes = req.body.columns;
      }
      if (req.body.where) {
        queryConditions.where = req.body.where;
      }
    }
    const selectedColumnsWhereEntries = await Entry.findAll(queryConditions);
    res.status(200).json({
      message: `selected columns: ${
        req.body.columns ? req.body.columns.toString() : 'all'
      } where ${
        req.body.where
          ? `${Object.keys(req.body.where)} = ${Object.values(req.body.where)}`
          : 'all'
      }`,
      data: selectedColumnsWhereEntries,
    });
  } catch (error) {
    next(error);
  }
};

exports.updateEntry = async (req, res, next) => {
  try {
    req.body.data = {
      ...req.body.data,
      updatedAt: moment().format(DATETIME_FORMAT),
    };
    const updatedRows = await Entry.update(req.body.data, {
      where: req.body.where,
    });
    res.status(202).json({
      message: updatedRows[0]
        ? `updated entry where ${Object.keys(req.body.where)} = ${Object.values(
            req.body.where
          )}`
        : 'nothing was updated',
      updatedRows: updatedRows[0],
      data: req.body.data,
    });
  } catch (error) {
    next(error);
  }
};

exports.deleteEntryWhereEntryId = async (req, res, next) => {
  try {
    const deletedRows = await Entry.destroy({
      where: {
        entryId: req.params.entryId,
      },
    });
    res.status(202).json({
      message: deletedRows
        ? `deleted user ${req.params.entryId}`
        : `no entryId: ${req.params.entryId}`,
      deletedRows,
    });
  } catch (error) {
    next(error);
  }
};

exports.deleteEntriesWhere = async (req, res, next) => {
  try {
    const deletedEntry = await Entry.destroy({
      where: req.body.where,
    });
    res.status(203).json({
      message: `deleted entries where ${Object.keys(
        req.body.where
      )} = ${Object.values(req.body.where)}`,
      data: deletedEntry,
    });
  } catch (error) {
    next(error);
  }
};
