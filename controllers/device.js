const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const Device = require('../models/checkAppDevice');
const User = require('../models/user');

const throwError = (message, status) => {
  const error = new Error(message);
  error.status = status;
  throw error;
};

exports.login = async (req, res, next) => {
  try {
    const device = await Device.findOne({
      where: {
        devicename: req.body.devicename,
      },
    });

    if (!device) {
      throwError('device not found!', 404);
    }

    const matched = bcrypt.compareSync(req.body.password, device.password);

    if (!matched) {
      throwError('wrong device password!', 400);
    }

    const token = jwt.sign(
      {
        checkAppDeviceId: device.checkAppDeviceId,
        devicename: device.devicename,
      },
      process.env.DEVICETOKEN,
      { expiresIn: '1h' }
    );
    await Device.update({ token: token }, {
      where: {
        devicename: req.body.devicename,
      }
    });
    res.status(200).json({
      message: 'login sucess',
      data: {
        token,
      },
    });
  } catch (next) { }
};

exports.selectUsers = async (req, res, next) => {


  try {

    const device = await Device.findOne({
      where: {
        devicename: req.body.devicename,
      },
    });

    var token = req.headers["authorization"].split(" ")[1];

    const selectedColumnsWhereUsers = await User.findAll();

    if (token == device.token) {
      res.status(200).json({
        message: "Success",
        data: selectedColumnsWhereUsers
      });
    } else {
      res.status(422).json({
        //log this if the device exists not "" then the jwt just expired , if not someonebody tried to access the api.
        message: "Unauthorized Access",
      });
    }
  } catch (next) {
    next(error);
  }
};

exports.logout = async (req, res) => { };
