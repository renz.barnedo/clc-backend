const bcrypt = require('bcryptjs');
const Role = require('../models/role');
const jwt = require('jsonwebtoken');

const throwError = (message, status) => {
  const error = new Error(message);
  error.status = status;
  throw error;
};

exports.login = async (req, res, next) => {
  try {
    const user = await Role.findOne({
      where: {
        username: req.body.username,
      },
    });

    if (!user) {
      throwError('user not found!', 404);
    }

    const matched = bcrypt.compareSync(req.body.password, user.password);

    if (!matched) {
      throwError('wrong password!', 400);
    }

    const token = jwt.sign(
      {
        userId: user.userId,
        role: user.role,
      },
      process.env.TOKEN,
      { expiresIn: '1h' }
    );

    res.status(200).json({
      message: 'login sucess!',
      data: {
        token,
      },
    });
  } catch (error) {
    next(error);
  }
};

exports.logout = async (req, res) => {};
