const User = require('../models/user');
const Role = require('../models/role');
const bcrpyt = require('bcryptjs');

const insertRole = async (role) => {
  try {
    const insertedRole = await Role.create(role);
    console.log('--------------ROLE INSERTED----------');
    console.log(insertedRole.dataValues);
    console.log('--------------ROLE INSERTED----------');
  } catch (error) {
    next(error);
  }
};

const createRole = (user) => {
  const { userId, role, firstName, lastName, suffixName, updatedBy } = user;

  const password = process.env.DEFAULTUSERPASSWORD;
  const saltRounds = 12;
  const hashedPassword = bcrpyt.hashSync(password, saltRounds);

  return {
    userId,
    role,
    username: `${
      firstName.toLowerCase()[0]
    }${lastName.toLowerCase()}${suffixName.toLowerCase()}`,
    password: hashedPassword,
    updatedBy,
  };
  // const didMatch = bcrpyt.compareSync(password, hashedPassword);
};

exports.insertUser = async (req, res, next) => {
  try {
    const insertedUser = await User.create(req.body.data.user);
    const role = createRole({
      ...insertedUser.dataValues,
      ...req.body.data.role,
    });

    res.status(201).json({
      message: 'inserted user',
      data: insertedUser,
      role,
    });

    insertRole(role);
  } catch (error) {
    next(error);
  }
};

exports.insertUsers = async (req, res, next) => {
  try {
    let users = req.body.data.map((user) => user.user);
    const insertedUsers = await User.bulkCreate(users);
    console.log(insertedUsers);
    users = insertedUsers.map((person, index) => {
      const role = createRole({
        ...person.dataValues,
        ...req.body.data[index].roles,
      });
      insertRole(role);
      return {
        user: person,
        role,
      };
    });

    res.status(201).json({
      message: 'inserted users',
      data: users,
    });
  } catch (error) {
    next(error);
  }
};

exports.selectColumnsWhereUsers = async (req, res, next) => {
  try {
    let queryConditions = {};
    if (req.body) {
      if (req.body.columns) {
        queryConditions.attributes = req.body.columns;
      }
      if (req.body.where) {
        queryConditions.where = req.body.where;
      }
    }
    const selectedColumnsWhereUsers = await User.findAll(queryConditions);
    res.status(200).json({
      message: `selected columns: ${
        req.body.columns ? req.body.columns.toString() : 'all'
      } where ${
        req.body.where
          ? `${Object.keys(req.body.where)} = ${Object.values(req.body.where)}`
          : 'all'
      }`,
      data: selectedColumnsWhereUsers,
    });
  } catch (next) {
    next(error);
  }
};

exports.selectUserWithUserId = async (req, res, next) => {
  try {
    const selectedUser = await User.findByPk(req.params.userId);
    res.status(200).json({
      message: `selected user with userId: ${req.params.userId}`,
      data: selectedUser,
    });
  } catch (error) {
    next(error);
  }
};

exports.updateUser = async (req, res, next) => {
  try {
    req.body.data = { ...req.body.data, updatedAt: new Date() };
    const updatedRows = await User.update(req.body.data, {
      where: req.body.where,
    });
    res.status(202).json({
      message: updatedRows[0]
        ? `updated user where ${Object.keys(req.body.where)} = ${Object.values(
            req.body.where
          )}`
        : 'nothing was updated',
      updatedRows: updatedRows[0],
      data: req.body.data,
    });
  } catch (error) {
    next(error);
  }
};

exports.deleteUserWhereUserId = async (req, res, next) => {
  try {
    const deletedRows = await User.destroy({
      where: {
        userId: req.params.userId,
      },
    });
    res.status(202).json({
      message: deletedRows
        ? `deleted user ${req.params.userId}`
        : `no userId: ${req.params.userId}`,
      deletedRows,
    });
  } catch (error) {
    next(error);
  }
};

exports.deleteUsersWhere = async (req, res, next) => {
  try {
    const deletedUser = await User.destroy({
      where: req.body.where,
    });
    res.status(203).json({
      message: `deleted users where ${Object.keys(
        req.body.where
      )} = ${Object.values(req.body.where)}`,
      data: deletedUser,
    });
  } catch (error) {
    next(error);
  }
};
